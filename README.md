# DataStax Enterprise Spark CICD
This repository represents a sample Spark application which runs on top of the [DataStax Enterprise](http://www.datastax.com) platform. The Spark application shouldn't be what you're really here for though. Ideally everything interesting lives in `.gitlab-ci.yml`. If you _are_ interested in checking out the application it's a copy of the Spark SBT example from the DataStax [SparkBuildExamples](https://github.com/datastax/SparkBuildExamples) on GitHub.

"Why?" you may be asking yourself. Well it contains all steps necessary to:

1. Build
2. Test
3. Assemble
4. Deploy

the application it shares a repository with. This allows for the usage of Gitlab's [CI / CD](https://docs.gitlab.com/ee/ci/yaml/) interfaces to handle launching of jobs within a number of environments. Since this lives with the Spark code changes may be made and promoted alongside the application logic.

# Setting up a local Docker environment

```bash
# Pull images down
docker pull gitlab/gitlab-ce:11.8.1-ce.0
docker pull gitlab/gitlab-runner:v11.8.0
docker pull datastax/dse-server:6.7.2

# Create private network for Gitlab
docker network create --attachable gitlab

# Create Gitlab instance
docker create \
  --name gitlab \
  --network gitlab \
  --volume /opt/gitlab/config:/etc/gitlab:Z \
  --volume /opt/gitlab/logs:/var/log/gitlab:Z \
  --volume /opt/gitlab/data:/var/opt/gitlab:Z \
  gitlab/gitlab-ce:11.8.1-ce.0
docker start gitlab

# Find the Gitlab IP address
GITLAB_IP=docker inspect gitlab | jq -r '.[0].NetworkSettings.Networks.gitlab.IPAddress'
sudo echo "$GITLAB_IP gitlab" >> /etc/hosts

# Create Gitlab Runner configuration
docker run -i -t -v --rm \
  --name gitlab-runner \
  --network gitlab \
  --volume /opt/gitlab-runner/config/:/etc/gitlab-runner:Z \
  gitlab/gitlab-runner:gitlab-runner:v11.8.0 \
  register

# Create Runner instance
docker create \
  --name gitlab-runner \
  --network gitlab \
  --volume /var/run/docker.sock:/var/run/docker.sock \
  --volume /opt/gitlab-runner/config:/etc/gitlab-runner:Z \
  gitlab/gitlab-runner:v11.8.0
docker start gitlab-runner

# Create target instance
docker create \
  --name dse-6.7.x-node-0 \
  --network gitlab \
  -e DS_LICENSE=accept \
  datastax/dse-server:6.7.2 \
  -k
docker start dse-6.7.x-node-0
```
